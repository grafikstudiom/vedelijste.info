<?php
	if(isset($_GET['content']) && $_GET['content']!=''){
		$data=file_get_contents("http://viteze.grafikstudio-m.com/data.php?content=".$_GET['content']);
		$data=json_decode($data);
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<meta property="og:type" content="article" />

<title>Víte, že <?=@$data[2]?></title>
<meta name="twitter:title" content="Víte, že <?=@$data[2]?>">
<meta property="og:title" content="Víte, že <?=@$data[2]?>" />

<link rel="stylesheet" type="text/css" href="/style.css">
<script src="http://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>

<meta name="description" content="<?=@$data[2]?>" />
<meta name="twitter:description" content="<?=@$data[2]?>">
<meta property="og:description" content="<?=@$data[2]?>" /> 

<meta name="keywords" content="Otázky, odpovědi, věděli jste, víte že" />
<meta property="og:image" content="/images/back/<?=@$data[4]?>" />
<meta name="twitter:image" content="/images/back/<?=@$data[4]?>">
<meta property="og:site_name" content="Víte, že..." />
</head>
<body>
<img class="bg jquery-img" src="" />
<div class="bg">&nbsp;</div>
<div id="content">
	<h2>Víte, že</h2>
    <h1 class="jquery-text"></h1>
    <div class="hastag jquery-hastag">
    </div>
    <br />
    <div class="addthis_sharing_toolbox"></div>
    <button class="next">Další</button>
</div>

<div id="footer">
	<a href="http://www.grafikstudio-m.com" title="Grafické a webové studio Grafik studio M"><img src="/images/gsm.png" alt="Grafik studio M logo" /></a>
</div>
<script>
var hastag='<?php if(isset($_GET['hastag'])){echo $_GET['hastag'];} ?>';
var content='<?php if(isset($_GET['content'])){echo $_GET['content'];} ?>';

function loadData(){
	$.ajax({
		url : "/data.php",
		type: "POST",
		data : {'content': content, 'hastag':hastag},
		success:function(data, textStatus, jqXHR){
			var ret = jQuery.parseJSON(data);
			content=ret[1];
			$('.jquery-text').html(ret[2]).animate('fade', 'fast');
			document.title = 'Víte, že '+ret[2];
			$('.jquery-img').fadeOut('fast', function () {
				$('.jquery-img').attr("src", "/images/back/"+ret[4]);
				$('.jquery-img').fadeIn('fast');
			});
			//Rewrite URL
			if(hastag!=''){
				rewriteUrl("/hastag/"+hastag+"/"+content+".html");
			}else{
				rewriteUrl("/"+content+".html");
			}
			//Hastag
			$('.jquery-hastag').html('');
			var has=ret[3].split(',');
			$.each(has, function (i, val){
				if(hastag==val){
					dataClass="hastag-selected"	;
				}else{dataClass='hastag-select';}
				$('.jquery-hastag').append('<a href="/hastag/'+val+'.html" rel="hastag" data-hastag="'+val+'" class="'+dataClass+'">#'+val+'</a>&nbsp;');
			});
		},
		error: function(jqXHR, textStatus, errorThrown){
			alert(textStatus+": "+errorThrown);
		}
	});	
}
	
$('.next').click(function(a){
	content='';
	loadData();
});

$("body").keydown(function(e) {
  if(e.keyCode == 37) { // left
    content='';
	loadData()
  }
  else if(e.keyCode == 39) { // right
    content='';
	loadData()
  }
});

$( document ).on( "click", ".hastag-selected", function(a) {
	hastag='';
	content='';
	loadData();
	return false;
});

$( document ).on( "click", ".hastag-select", function(a) {
	hastag=$(this).attr('data-hastag');
 	changeUrl($(this).attr('href'));
	return false;
});
 
function changeUrl(pageurl){
	if(rewriteUrl(pageurl)){
		loadData();
	}
}
		
function rewriteUrl(pageurl){
	if(pageurl!=window.location){
		window.history.pushState({path:pageurl},'',pageurl);
		return true;
	}else{
		return false;
	}
}
	
$(document).on( "click", "a[rel='href']", function() {
 	changeUrl($(this).attr('href'));
	return false;
});

$(document).ready(function(e) {
    loadData();
});
</script>
<!-- Go to www.addthis.com/dashboard to customize your tools -->
<script type="text/javascript" src="http://s7.addthis.com/js/300/addthis_widget.js#pubid=ra-50d9e1ec37647f43"></script>
</body>
</html>
